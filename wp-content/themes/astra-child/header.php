<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?><!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
<?php astra_head_top(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:wght@600;700;800&family=Open+Sans:wght@400;600;700;800&display=swap" rel="stylesheet">
<link rel="profile" href="https://gmpg.org/xfn/11">

<?php wp_head(); ?>
<?php astra_head_bottom(); ?>
</head>

<body <?php astra_schema_body(); ?> <?php body_class(); ?>>
<?php astra_body_top(); ?>
<?php wp_body_open(); ?>
<script type="text/javascript">
	// reload page on basket update
	jQuery( document.body ).on( 'updated_cart_totals', function() {
	    location.reload();
	});
</script>
<div 
<?php
	echo astra_attr(
		'site',
		array(
			'id'    => 'page',
			'class' => 'hfeed site',
		)
	);
	?>
>
	<a class="skip-link screen-reader-text" href="#content"><?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?></a>
	<?php 
	astra_header_before(); 

	//astra_header(); ?>
	<header class="site-header header-main-layout-1 ast-primary-menu-enabled ast-has-mobile-header-logo ast-menu-toggle-icon ast-mobile-header-inline ast-above-header-mobile-inline ast-below-header-mobile-inline" id="masthead" itemtype="https://schema.org/WPHeader" itemscope="itemscope" itemid="#masthead">
		<div class="main-header-bar-wrap">
			<div <?php echo astra_attr( 'main-header-bar' ); ?>>
				<?php astra_main_header_bar_top(); ?>
				<div class="ast-container">
					<div class="ast-flex main-header-container">
						<div class="site-branding">
							<div
							<?php
								echo astra_attr(
									'site-identity',
									array(
										'class' => 'ast-site-identity',
									)
								);
							?>
							>
								<?php astra_logo(); ?>
							</div>
						</div>
						<div class="ast-mobile-menu-buttons">
							<?php astra_masthead_toggle_buttons_before(); ?>
							<?php astra_masthead_toggle_buttons(); ?>
							<?php astra_masthead_toggle_buttons_after(); ?>
						</div>
						<?php $submenu_class = apply_filters( 'primary_submenu_border_class', ' submenu-with-border' );
						// Menu
						$menu_animation = astra_get_option( 'header-main-submenu-container-animation' );
						if ( ! empty( $menu_animation ) ) {
							$submenu_class .= ' astra-menu-animation-' . esc_attr( $menu_animation ) . ' ';
						}
						/**
						 * Filter the classes(array) for Primary Menu (<ul>).
						 *
						 * @since  1.5.0
						 * @var Array
						 */
						$primary_menu_classes = apply_filters( 'astra_primary_menu_classes', array( 'main-header-menu', 'ast-nav-menu', 'ast-flex', 'ast-justify-content-flex-end', $submenu_class ) );
						// Fallback Menu if primary menu not set.
						$fallback_menu_args = array(
							'theme_location' => 'primary',
							'menu_id'        => 'primary-menu',
							'menu_class'     => 'main-navigation',
							'container'      => 'div',
							'before'         => '<ul class="' . esc_attr( implode( ' ', $primary_menu_classes ) ) . '">',
							'after'          => '</ul>',
							'walker'         => new Astra_Walker_Page(),
						);
						$items_wrap  = '<nav ';
						$items_wrap .= astra_attr(
							'site-navigation',
							array(
								'id'         => 'site-navigation',
								'class'      => 'ast-flex-grow-1 navigation-accessibility',
								'aria-label' => esc_attr__( 'Site Navigation', 'astra' ),
							)
						);
						$items_wrap .= '>';
						$items_wrap .= '<div class="main-navigation">';
						$items_wrap .= '<ul id="%1$s" class="%2$s">%3$s</ul>';
						$items_wrap .= '</div>';
						$items_wrap .= '</nav>';
						// Primary Menu.
						$primary_menu_args = array(
							'theme_location'  => 'primary',
							'menu_id'         => 'primary-menu',
							'menu_class'      => esc_attr( implode( ' ', $primary_menu_classes ) ),
							'container'       => 'div',
							'container_class' => 'main-header-bar-navigation',
							'items_wrap'      => $items_wrap,
						);
						if ( has_nav_menu( 'primary' ) ) {
							// To add default alignment for navigation which can be added through any third party plugin.
							// Do not add any CSS from theme except header alignment.
							echo '<div ' . astra_attr( 'ast-main-header-bar-alignment' ) . '>';
								wp_nav_menu( $primary_menu_args );
							echo '</div>';
						} else {
							echo '<div ' . astra_attr( 'ast-main-header-bar-alignment' ) . '>';
								echo '<div class="main-header-bar-navigation ast-flex-1">';
									echo '<nav ';
									echo astra_attr(
										'site-navigation',
										array(
											'id' => 'site-navigation',
										)
									);
									echo ' class="ast-flex-grow-1 navigation-accessibility" aria-label="' . esc_attr__( 'Site Navigation', 'astra' ) . '">';
										wp_page_menu( $fallback_menu_args );
									echo '</nav>';
								echo '</div>';
							echo '</div>';
						} // end menu ?>
						<div class="loginbuttons">
							<a href="<?= home_url().'/my-account'; ?>" class="login">LOGIN</a>
							<a href="<?= home_url().'/my-account'; ?>" class="signup">SIGN UP</a>
						</div>
						<a class="applepay" href="#"></a>
						<?php global $woocommerce;
						if($woocommerce->cart->cart_contents_count){
							$cartclass = 'not-empty';
							if($woocommerce->cart->cart_contents_count>1){
								$s = 's';
							}
							$cartcontent = '<span>'.$woocommerce->cart->cart_contents_count.' item'.$s.'</span>';
						} ?>
						<a href="<?= home_url().'/cart'; ?>" class="shopcart <?=$cartclass?>"><?=$cartcontent?></a>
						<a href="https://uk.trustpilot.com/review/hothatchgiveaways.co.uk" target="_blank" class="trusttpilot">
							<div class="logo"></div>
							<div class="star"></div>
							<div class="star"></div>
							<div class="star"></div>
							<div class="star"></div>
							<div class="star"></div>
						</a>
					</div><!-- Main Header Container -->
				</div><!-- ast-row -->
				<?php astra_main_header_bar_bottom(); ?>
				<?php // page title
				$post = $wp_query->get_queried_object();
				if($post->has_archive){
	  				$page_title = str_replace("-", " ", $post->has_archive);
	  			}else{
	  				$page_title = get_the_title();
	  			} ?>
				<h1 class="header-title"><?=$page_title?></h1>
			</div> <!-- Main Header Bar -->
		</div> <!-- Main Header Bar Wrap -->
	</header>
	

	<?php astra_header_after();

	astra_content_before(); 
	?>
	<div id="content" class="site-content">
		<div class="ast-container">
		<?php astra_content_top(); ?>
