<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

        <?php /* new acf modules */
        $carousel = get_field('carousel'); ?>
        <div id="topCarousel" class="carousel slide" data-bs-ride="carousel">
            <h1><?php echo $carousel['title']; ?></h1>
            <div class="carousel-inner">
                <?php $date_now = date("Y-m-d H:i:s");
                $i=0;
                foreach($carousel['slides'] as $slide){
                    if($date_now > $slide['start_date'] && $date_now < $slide['end_date']){ ?>
                        <div class="carousel-item <?= ($i==0)?'active':'' ?>">
                            <div class="bg_image" style="background-image: url(<?= $slide['image']['url']; ?>);">
                                <div class="shadowdeets">
                                    <div class="carname">
                                        <?= $slide['car_name']; ?>
                                    </div>
                                    <a href="<?= $slide['button']['url']; ?>" class="btn viewcomp" target="<?= $slide['button']['target']; ?>">
                                        <?= $slide['button']['title']; ?>
                                    </a>
                                </div>
                            </div>
                            <div class="details">
                                <div class="green">
                                    <div class="price">
                                        ENTER FOR: <span><?= $slide['ticket_price']; ?></span>
                                    </div>
                                </div>
                                <ul>
                                    <?php if(isset($slide['bhp']) && $slide['bhp'] != ''){ ?>
                                        <li class="bhp">
                                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/icon_bhp.svg">
                                            <div class="data">
                                                <label>BHP</label><br>
                                                <span><?= $slide['bhp']; ?></span>
                                            </div>
                                        </li>
                                    <?php } 
                                    if(isset($slide['zero_sixty']) && $slide['zero_sixty'] != ''){ ?>
                                        <li class="zero-sixty">
                                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/icon_speedo.svg">
                                            <div class="data">
                                                <label>0-60</label><br>
                                                <span><?= $slide['zero_sixty']; ?></span>
                                            </div>
                                        </li>
                                    <?php } 
                                    if(isset($slide['year']) && $slide['year'] != ''){ ?>
                                        <li class="year">
                                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/icon_calendar.svg">
                                            <div class="data">
                                                <label>YEAR</label><br>
                                                <span><?= $slide['year']; ?></span>
                                            </div>
                                        </li>
                                    <?php } 
                                    if(isset($slide['miles']) && $slide['miles'] != ''){ ?>
                                        <li class="miles">
                                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/icon_miles.svg">
                                            <div class="data">
                                                <label>MILES</label><br>
                                                <span><?= $slide['miles']; ?></span>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <?php $i++;
                    }
                } ?>
            </div>
            <div class="controlls">
                <div class="carousel-indicators">
                    <?php $i=$i-1;
                    for ($a = 0; $a <= $i; $a++) { ?>
                        <button type="button" data-bs-target="#topCarousel" data-bs-slide-to="<?=$a?>" <?= ($a==0)?'class="active"':'' ?> aria-current="true" aria-label="Slide <?=($a+1);?>"></button>
                    <?php } ?>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#topCarousel" data-bs-slide="prev">
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#topCarousel" data-bs-slide="next">
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
        <div class="current-comps-title">
            <h3>CURRENT COMPETITIONS</h3>
        </div>
        <section class="current-comp">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?php $args = array(
                            'post_type' => 'product',
                            'posts_per_page' => -1
                            );
                        $loop = new WP_Query($args);
                        $total = $loop->post_count; ?>
                        <div id="current-comp-carousel" class="carousel slide full-size-only" data-bs-ride="carousel">
                            <div class="carousel-indicators">
                                <?php $i=0; $j=0;
                                while($loop->have_posts()){ $loop->the_post();
                                    if(($i+1) != $total){
                                        if($i==0||$i%3==2){ ?>
                                            <button type="button" data-bs-target="#current-comp-carousel" data-bs-slide-to="<?=$j?>" class="<?= ($j==0)?'active':'' ?>" aria-current="true" aria-label="Slide <?=($j+1);?>"></button>
                                            <?php $j++;
                                        }
                                    }
                                    $i++;
                                } ?>                
                            </div>
                            <div class="carousel-inner">
                                <?php if($loop->have_posts()){
                                    $i=0; ?>
                                    <div class="carousel-item active">
                                        <div class="row">
                                            <?php while($loop->have_posts()){ $loop->the_post(); ?>
                                                <div class="col-sm-4">
                                                    <div class="product">
                                                        <?php global $product; ?>
                                                        <a href="<?= the_permalink(); ?>">
                                                            <img src="<?=wp_get_attachment_url($product->get_image_id())?>" width="100%">
                                                        </a>
                                                        <div class="details">
                                                            <a href="<?= the_permalink(); ?>">
                                                                <h4><?= the_title(); ?></h4>
                                                            </a>
                                                            <a href="<?= the_permalink(); ?>">
                                                                <div class="price">
                                                                    <?php $product = wc_get_product(get_the_ID());
                                                                    echo $product->get_price_html(); ?>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <?php $lottery_date_to = get_post_meta($product->id, '_lottery_dates_to', true);
                                                        $date1 = new DateTime($lottery_date_to);
                                                        $date2 = new DateTime();
                                                        $interval = $date1->diff($date2);
                                                        $days_to_end = $interval->days; ?>
                                                        <div class="green">
                                                            <div class="days">
                                                                <?= $days_to_end; ?> DAYS<br>
                                                                <span>UNTIL DRAW</span>
                                                            </div>
                                                            <div class="tickets">
                                                                <?php $max_tickets = $product->get_max_tickets();
                                                                $lottery_participants_count = !empty($product->get_lottery_participants_count()) ? $product->get_lottery_participants_count() : '0';
                                                                $tickets_left = $max_tickets-$lottery_participants_count; ?>
                                                                <?=$tickets_left?><br>
                                                                <span>TICKETS REMAINING</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php // close and open container after every three, unless this is the last post
                                                if(($i+1) != $total){
                                                    if($i%3==2){echo '</div></div><div class="carousel-item"><div class="row">';}
                                                }
                                                $i++;
                                            } ?>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <p>No competitions currently running</p>
                                <?php }
                                wp_reset_postdata(); ?>
                            </div>
                        </div>

                        <div id="current-comp-carousel-mobile" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-indicators">
                                <?php $i=0;
                                while($loop->have_posts()){ $loop->the_post(); ?>
                                    <button type="button" data-bs-target="#current-comp-carousel-mobile" data-bs-slide-to="<?=$i?>" class="<?= ($i==0)?'active':'' ?>" aria-current="true" aria-label="Slide <?=($i+1);?>"></button>
                                    <?php $i++;
                                } ?>                
                            </div>
                            <div class="carousel-inner">
                                <?php if($loop->have_posts()){
                                    $i=0;
                                    while($loop->have_posts()){ $loop->the_post(); ?>
                                        <div class="carousel-item <?=($i==0)?'active':''?>">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="product">
                                                        <?php global $product; ?>
                                                        <a href="<?= the_permalink(); ?>">
                                                            <img src="<?=wp_get_attachment_url($product->get_image_id())?>" width="100%">
                                                        </a>
                                                        <div class="details">
                                                            <a href="<?= the_permalink(); ?>">
                                                                <h4><?= the_title(); ?></h4>
                                                            </a>
                                                            <a href="<?= the_permalink(); ?>">
                                                                <div class="price">
                                                                    <?php $product = wc_get_product(get_the_ID());
                                                                    echo $product->get_price_html(); ?>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <?php $lottery_date_to = get_post_meta($product->id, '_lottery_dates_to', true);
                                                        $date1 = new DateTime($lottery_date_to);
                                                        $date2 = new DateTime();
                                                        $interval = $date1->diff($date2);
                                                        $days_to_end = $interval->days; ?>
                                                        <div class="green">
                                                            <div class="days">
                                                                <?= $days_to_end; ?> DAYS<br>
                                                                <span>UNTIL DRAW</span>
                                                            </div>
                                                            <div class="tickets">
                                                                <?php $max_tickets = $product->get_max_tickets();
                                                                $lottery_participants_count = !empty($product->get_lottery_participants_count()) ? $product->get_lottery_participants_count() : '0';
                                                                $tickets_left = $max_tickets-$lottery_participants_count; ?>
                                                                <?=$tickets_left?><br>
                                                                <span>TICKETS REMAINING</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $i++;
                                    }
                                }else{ ?>
                                    <p>No competitions currently running</p>
                                <?php }
                                wp_reset_postdata(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>





        <?php /* end new acf modules */ ?>

		<?php astra_primary_content_top(); ?>

		<?php astra_content_page_loop(); ?>

		<?php astra_primary_content_bottom(); ?>




        <section id="winners">
            <?php $winners_wall_of_fame = get_field('winners_wall_of_fame'); ?>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3><?= $winners_wall_of_fame['title']; ?></h3>
                        
                        <div id="winners-carousel" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-indicators">
                                <?php $i=0; $j=0;
                                $total = count($winners_wall_of_fame['winners']);
                                foreach($winners_wall_of_fame['winners'] as $i => $winner){
                                    // TO DO : get total
                                    if(($i+1) != $total){
                                        if($i==0||$i%4==3){ ?>
                                            <button type="button" data-bs-target="#winners-carousel" data-bs-slide-to="<?=$j?>" class="<?= ($j==0)?'active':'' ?>" aria-current="true" aria-label="Slide <?=($j+1);?>"></button>
                                            <?php $j++;
                                        }
                                    }
                                    $i++;
                                } ?>                
                            </div>
                            <div class="carousel-inner">
                                <?php if($winners_wall_of_fame['winners']){
                                    $i=0; ?>
                                    <div class="carousel-item active">
                                        <div class="row">
                                            <?php foreach($winners_wall_of_fame['winners'] as $i => $winner){ ?>
                                                <div class="col-sm-3">
                                                    <a class="winner" href="<?= $winner['link']; ?>">
                                                        <div class="image" style="background-image: url(<?=$winner['image']['url']; ?>);"></div>
                                                        <div class="details">
                                                            <h4>
                                                                <?php echo $winner['name'];
                                                                if(isset($winner['location']) && $winner['location'] != ''){
                                                                    echo " from ".$winner['location'];
                                                                } ?>
                                                            </h4>
                                                            <div class="prize">
                                                                <?=$winner['prize'];?>
                                                            </div>
                                                            <div class="date">
                                                                <?=$winner['date'];?>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <?php // close and open container after every three, unless this is the last post
                                                if(($i+1) != $total){
                                                    if($i%4==3){echo '</div></div><div class="carousel-item"><div class="row">';}
                                                }
                                                $i++;
                                            } ?>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <p>No winners yet</p>
                                <?php }
                                wp_reset_postdata(); ?>
                            </div>
                        </div>

                        <div id="winners-carousel-mobile" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-indicators">
                                <?php $i=0;
                                foreach($winners_wall_of_fame['winners'] as $i => $winner){ ?>
                                    <button type="button" data-bs-target="#winners-carousel-mobile" data-bs-slide-to="<?=$i?>" class="<?= ($i==0)?'active':'' ?>" aria-current="true" aria-label="Slide <?=($i+1);?>"></button>
                                    <?php $i++;
                                } ?>                
                            </div>
                            <div class="carousel-inner">
                                <?php if($winners_wall_of_fame['winners']){
                                    $i=0;
                                    foreach($winners_wall_of_fame['winners'] as $i => $winner){ ?>
                                        <div class="carousel-item <?=($i==0)?'active':''?>">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <a class="winner" href="<?= $winner['link']; ?>">
                                                        <div class="image" style="background-image: url(<?=$winner['image']['url']; ?>);"></div>
                                                        <div class="details">
                                                            <h4>
                                                                <?php echo $winner['name'];
                                                                if(isset($winner['location']) && $winner['location'] != ''){
                                                                    echo " from ".$winner['location'];
                                                                } ?>
                                                            </h4>
                                                            <div class="prize">
                                                                <?=$winner['prize'];?>
                                                            </div>
                                                            <div class="date">
                                                                <?=$winner['date'];?>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                }else{ ?>
                                    <p>No winners yet</p>
                                <?php }
                                wp_reset_postdata(); ?>
                            </div>
                        </div>
                    </div>
                    <?php if($winners_wall_of_fame['all_winners_link']){ ?>
                        <a href="<?=$winners_wall_of_fame['all_winners_link']['url'] ?>" class="btn"><?=$winners_wall_of_fame['all_winners_link']['title'] ?></a>
                    <?php } ?>
                </div>
            </div>
        </section>





	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
