jQuery(function($){

	$(".pt-cv-ifield").each(function() {
		// move date element
		$(this).find('.pt-cv-meta-fields').appendTo(this);
		// change date format
		var dateObject = $(this).find('.entry-date time').attr('datetime');
		var d = new Date(dateObject);
	    var day = d.getDate();
	    var month = d.getMonth() + 1;
	    var year = d.getFullYear();
	    if (day < 10) {
	        day = "0" + day;
	    }
	    if (month < 10) {
	        month = "0" + month;
	    }
	    var date = day + "/" + month + "/" + year;
	    $(this).find('.entry-date time').html(date);
	});

	$('.woocommerce-account .woocommerce .woocommerce-form-login').siblings('h2').attr('id','login-title-label');
	$('.woocommerce-account .woocommerce .woocommerce-form-register').siblings('h2').attr('id','register-title-label');

});