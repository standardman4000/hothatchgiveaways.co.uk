<?php

// disable gutenberg
add_filter('use_block_editor_for_post', '__return_false', 10);


wp_enqueue_style( 'bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css',false,'1.1','all');
wp_enqueue_script( 'bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js', array ( 'jquery' ), 1.1, true);

add_action( 'wp_enqueue_scripts', 'astra_child_theme_enqueue_styles', 20 );
function astra_child_theme_enqueue_styles() {
    wp_enqueue_style( 'child', get_stylesheet_directory_uri().'/style.css', 'wc-lottery-css', wp_get_theme()->get('Version'));
}

 /* ORIGINAL FUNCTION 
add_action('template_redirect','check_if_logged_in');
function check_if_logged_in()
{
    $pageid = 97;
   if(!is_user_logged_in() && is_page($pageid))
    {
        $url = add_query_arg(
            'redirect_to',
            get_permalink($pagid),
            site_url('/my-account/') // your my acount url
        );
        wp_redirect($url);
        exit;
    } 
}
*/


/* MY FUNCTION */
add_action('template_redirect', 'woocommerce_custom_redirections');
function woocommerce_custom_redirections() {
    // Case1: Non logged user on checkout page (cart not empty)
    if ( !is_user_logged_in() && is_checkout() && !WC()->cart->is_empty() )
        wp_redirect( get_permalink( get_option('woocommerce_myaccount_page_id') ) );
    // Case1: Non logged user on cart page (cart empty)
    if ( !is_user_logged_in() && is_cart() && WC()->cart->is_empty() )
        wp_redirect( get_permalink( get_option('woocommerce_myaccount_page_id') ) );
}

//if(is_page(2850)){
    wp_enqueue_script('page_helper', get_stylesheet_directory_uri().'/assets/js/page_helper.js', array('jquery'), 1.1, true);
//}



// my account visual hook for showing notification
add_action('woocommerce_before_customer_login_form', 'show_mobile_buttons');
function show_mobile_buttons(){ ?>
    <div class="login-register-message">
        <p>You must be registered to enter our competitions. Please login or register an account below.</p>
        <div class="login-register-buttons">
            <a href="#login-title-label">Login</a>
            <a href="#register-title-label">Register</a>
        </div>
    </div>
<?php }
    /*
    $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    if (strpos($url,'cart') !== false) { ?>
        <div class="alert alert-warning" role="alert">
            <p>You must be registered to enter our competitions. Please login or register an account below.</p>
            <div class="login-register-buttons">
                <a href="#login-title-label">Login</a>
                <a href="#register-title-label">Register</a>
            </div>
        </div>
    <?php }
    */




// create shortcode for forgot password form
function wc_custom_lost_password_form( $atts ) {
    return wc_get_template( 'myaccount/form-lost-password.php', array( 'form' => 'lost_password' ) );
}
add_shortcode( 'lost_password_form', 'wc_custom_lost_password_form' );




 
// change sender name
function wpb_sender_name( $original_email_from ) {
    return 'Hot Hatch Giveaways';
}
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );