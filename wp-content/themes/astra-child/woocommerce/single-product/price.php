<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<p class="price-note">You must answer the question correctly when you enter. For free postal entry route <a href="#" data-toggle="modal" data-target="#postalModal">see here</a>.</p>
<!-- Modal -->
<div class="modal fade" id="postalModal" tabindex="-1" aria-labelledby="postalModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="postalModalLabel">Postal Entry Route</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ol>
                    <li><span>In order to take advantage of a free ticket, you must send the following details on an unenclosed postcard via First Class post to HOT HATCH GIVEAWAYS LIMITED, Unit 32517, PO Box 6945, London, W1A 6US:</span>
                        <ol>
                            <li>the title of the Competition you would like to enter;</li>
                            <li>the answer to the question of the respective Competition;</li>
                            <li>your full name;</li>
                            <li>your date of birth;</li>
                            <li>your postal address, email address and contact telephone number.</li>
                        </ol>
                    </li>
                    <li>Entrants must register an account on our Website before sending your postal Entry. All details on the postal Entry must be clearly visible and must match the details of your registered account. Failure to read your postal Entry clearly will result in the Entry being void. All postal entries must be received before the end date of the Competition.</li>
                    <li>All postal entries are processed in the same manner as entries from the Website once they are received from our PO Box provider.</li>
                    <li>The maximum number of Free Postal entries is 1 per person per household.</li>
                    <li>Clauses 4.4.1 & 4.4.2 from our terms and conditions apply and any correct answer will be treated as an Entry with the same terms and conditions as a paid ticket.</li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>">ENTER FOR: <span><?php echo $product->get_price_html(); ?></span></p>
