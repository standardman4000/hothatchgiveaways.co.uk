<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters(
	'woocommerce_single_product_image_gallery_classes',
	array(
		'woocommerce-product-gallery',
		'woocommerce-product-gallery--' . ( $product->get_image_id() ? 'with-images' : 'without-images' ),
		'woocommerce-product-gallery--columns-' . absint( $columns ),
		'images',
	)
);
?>
<h1 class="mobile-only mobile-product-title"><?= the_title(); ?></h1>
<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
	<figure class="woocommerce-product-gallery__wrapper">
		<?php
		if ( $product->get_image_id() ) {
			$html = wc_get_gallery_image_html( $post_thumbnail_id, true );
		} else {
			$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
			$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src( 'woocommerce_single' ) ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
			$html .= '</div>';
		}

		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id ); // phpcs:disable WordPress.XSS.EscapeOutput.OutputNotEscaped

		do_action( 'woocommerce_product_thumbnails' );
		?>
	</figure>
</div>
<div class="specs">
	<?php $spec = get_field('car_specs'); ?>
	<ul>
		<?php if(isset($spec['bhp']) && $spec['bhp'] != ''){ ?>
			<li class="bhp">
				<div class="icon"></div>
				<div class="info">BHP<br><span><?= $spec['bhp']; ?></span></div>
			</li>
		<?php }
		if(isset($spec['zero_sixty']) && $spec['zero_sixty'] != ''){ ?>
			<li class="zero_sixty">
				<div class="icon"></div>
				<div class="info">0-60<br><span><?= $spec['zero_sixty']; ?></span></div>
			</li>
		<?php }
		if(isset($spec['year']) && $spec['year'] != ''){ ?>
			<li class="year">
				<div class="icon"></div>
				<div class="info">YEAR<br><span><?= $spec['year']; ?></span></div>
			</li>
		<?php }
		if(isset($spec['miles']) && $spec['miles'] != ''){ ?>
			<li class="miles">
				<div class="icon"></div>
				<div class="info">MILES<br><span><?= $spec['miles']; ?></span></div>
			</li>
		<?php } ?>
	</ul>		
</div>
<script type="text/javascript">
	jQuery(function($){
		// delay for 10th of a sec to wait for image gallery to arrive
		function delay(){
			// move specs
			$('.specs').appendTo(".woocommerce-product-gallery");
			// wrap images to convert into carousel
			$("ol.flex-control-nav").wrap("<div class='gallery-carousel'><div class='carousel-inside'></div></div>");
			$('.gallery-carousel').prepend('<div class="prev"></div>');
			$('.gallery-carousel').append('<div class="next"></div>');
			// carousel next/prev controls
			$(".next").click(function(){
				var left = parseInt($('ol.flex-control-nav').css("left"));
				var olwidth = $('ol.flex-control-nav').width();
				olwidth = -Math.abs(olwidth);
				if(olwidth < (left-300)){
					$('ol.flex-control-nav').css('left', left-200);
				}
			});
			$(".prev").click(function(){
				var left = parseInt($('ol.flex-control-nav').css("left"));
				if(left < -200){
					$('ol.flex-control-nav').css('left', left+200);
				}else{
					$('ol.flex-control-nav').css('left', 0);
				}
			});
		}
		window.setTimeout(delay, 100);
			
	});
</script>