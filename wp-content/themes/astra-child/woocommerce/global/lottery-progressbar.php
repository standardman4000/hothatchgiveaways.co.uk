<?php
/**
 * Lottery progressbar template
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global  $product, $post;
$min_tickets                = $product->get_min_tickets();
$max_tickets                = $product->get_max_tickets();
$max_per_person             = $product->get_max_tickets_per_user();
$lottery_participants_count = !empty($product->get_lottery_participants_count()) ? $product->get_lottery_participants_count() : '0';
?>

<?php if( $max_tickets  &&( $max_tickets > 0 )  && (get_option( 'simple_lottery_progressbar' ,'yes' ) == 'yes') ) : ?>
    <div class="wcl-progress-meter <?php if($product->is_max_tickets_met()) echo 'full' ?>">
        <div class="title">
            <p>TICKETS<span class="ticket-icon"></span></p>
        </div>
        <span class="zero"><b><?php echo $lottery_participants_count ?></b> SOLD</span>
        <span class="max">REMAINING <b><?php echo ($max_tickets-$lottery_participants_count); ?></b></span>
        <progress  max="<?php echo $max_tickets ?>" value="<?php echo $lottery_participants_count ?>"  low="<?php echo $min_tickets ?>"></progress>
        <div class="entry-info">
            <div class="max-entries">
                <p><span class="icon"></span>Max entries: <span><?=$max_tickets?></span></p>
            </div>
            <div class="max-per-person">
                <p><span class="icon"></span>Max <span><?=$max_per_person?></span> per person</p>
            </div>
        </div>
    </div>
<?php endif; ?>
