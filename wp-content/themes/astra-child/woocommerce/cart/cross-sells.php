<?php
/**
 * Cross-sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cross-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.4.0
 */

defined( 'ABSPATH' ) || exit;

if ( $cross_sells ) : ?>

	<div class="cross-sells">
		<?php
		$heading = apply_filters( 'woocommerce_product_cross_sells_products_heading', __( 'You may be interested in&hellip;', 'woocommerce' ) );

		if ( $heading ) :
			?>
			<h2><?php echo esc_html( $heading ); ?></h2>
		<?php endif; ?>

		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $cross_sells as $cross_sell ) : ?>

				<?php
					$post_object = get_post( $cross_sell->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

					//wc_get_template_part( 'content', 'product' );

					$product = wc_get_product($post_object->ID);
				?>


			
				<li class="alex ast-article-single align-center box-shadow-2 box-shadow-3-hover ast-product-gallery-layout-horizontal ast-product-gallery-with-no-image ast-product-tabs-layout-vertical product type-product post-14493 status-publish first instock product_cat-bundles has-post-thumbnail shipping-taxable purchasable product-type-lottery">
					<div class="astra-shop-thumbnail-wrap">
						<a href="https://hothatchgiveaways.co.uk/current-competitions/tamiya-escort-mk-ii-rally/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
							<img width="300" height="300" src="<?=wp_get_attachment_url($product->get_image_id())?>">
						</a>
					</div>
					<div class="astra-shop-summary-wrap">
						<a href="https://hothatchgiveaways.co.uk/current-competitions/tamiya-escort-mk-ii-rally/" class="ast-loop-product__link">
							<h2 class="woocommerce-loop-product__title">Add this: <?=$post_object->post_title?> for just <span>£<?=$product->get_price()?></span></h2>
						</a>
					</div>
				</li>


			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</div>
	<?php
endif;

wp_reset_postdata();
